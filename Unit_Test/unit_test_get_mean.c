#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../gets.c"

void set_array_nil(int size_int_,double spin_ary_[size_int_]){
  int i=0;
  for(i=0; i<size_int_; i++){
		spin_ary_[i]=2.0;
  }
}

int main(){
	int i=0;
	int size_int=10;
	double ary_2[size_int];
	double *ary_ptr_2=NULL;

	ary_ptr_2=malloc(size_int*sizeof(double));

	for(i=0; i<size_int; ++i){
		ary_2[i]=2.0;
	}

	set_array_nil(size_int,ary_2);
	set_array_nil(size_int,ary_ptr_2);
	
	for(i=0; i<size_int; ++i){
		printf("%.2lf ",ary_2[i]);
	}
	printf("\n");
	for(i=0; i<size_int; ++i){
		printf("%.2lf ", ary_ptr_2[i]);
	}

	printf("\n");
	printf("avg ary_2:\t%.2lf\n", get_mean(size_int, ary_2) );
	printf("avg ary_ptr_2:\t%.2lf\n", get_mean(size_int, ary_ptr_2) );

	free(ary_ptr_2);
	return 0;
}